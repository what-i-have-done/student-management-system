#define MAXSTU 200
#define MAXNAME 128
#define MAXPWD 32

struct grade
{
    int scores[3];
    int rank;
};

struct student
{
    int number; // 学号
    char name[MAXNAME]; // 姓名
    char password[MAXPWD]; // 密码
    int age;
    int stu_class;
    struct grade stu_grade;
};

extern void read_students(struct student *);
extern void write_students(struct student *);
extern void add_students(struct student *);
extern void print_students(struct student *);
extern void print_student_header();
extern void print_a_student(struct student *);
extern int seek_student_by_number(struct student *, int number);
extern int seek_students_by_name(struct student *, char * name);
extern int seek_students_by_class(struct student *, int);
extern void modify_student_by_location(struct student *, int);
extern void sort_students_by_number(struct student *);
extern int sum_scores_by_subject(struct student *, int);
extern void sort_students_by_scores(struct student *);
extern void sort_students_by_subject(struct student *, int);
extern void rank_students_by_scores(struct student *);
extern int delete_student_by_number(struct student *, int);
extern void copy_students_array(struct student *, struct student *);

extern int stucount;
