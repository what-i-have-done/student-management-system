#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "students.h"
#include "teachers.h"
#include "login.h"
#include "utils.h"

int is_teacher = 0; /* 老师标志位 */
int is_admin = 0; /* 管理员标志位 */

void seek_students(struct student *);
void show_students(struct student *);
void student_menu(struct student *, struct teacher *, int);
void student_menu_by_teacher(struct student *, struct teacher *, int);
void student_menu_by_student(struct student *, int);
void teacher_menu(struct teacher *, int);
void teacher_menu_by_admin(struct teacher *);
void teacher_menu_by_teacher(struct teacher *, int);
void teacher_student_menu(struct teacher *, struct student *, int);

void admin_menu(struct student * students, struct teacher * teachers)
{

    admin_login();
    teacher_student_menu(teachers, students, -1); /* 管理员登入， 没有老师和学生 */
}

void login_menu(struct student * students, struct teacher * teachers)
{
    while (1)
    {
        printf("== 登入 ==\n");
        puts("1 学生登入");
        puts("2 教师登入");
        printf("选择功能（按 q 退出）：");
        char ch = getchar();
        clear_input();

        if ('q' == ch)
            exit(EXIT_SUCCESS);

        int location = -1;
        switch (ch)
        {
            case '1':
                /* read_students(students); */
                location = student_login(students);
                student_menu(students, teachers, location);
                break;
            case '2':
                /* read_students(students); */
                /* read_teachers(teachers); */
                location = teacher_login(teachers);
                teacher_student_menu(teachers, students, location);
                break;
        }
    }
}

void student_menu(struct student * students, struct teacher * teachers, int loc)
{
    if (is_teacher)
    {
        student_menu_by_teacher(students, teachers, loc);
    }
    else
    {
        student_menu_by_student(students, loc);
    }

}

void student_menu_by_teacher(struct student * students, struct teacher * teachers, int loc)
{
    char ch;
    int number, ret_val;
    while (1)
    {
        printf("=== 学生菜单 ===\n");
        if (-1 != loc)
            printf("\t\t欢迎%s老师登入\n", teachers[loc].name);
        puts("1 录入学生信息");
        puts("2 删除学生信息");
        puts("3 修改学生信息");
        puts("4 查找学生信息");
        puts("5 显示所有学生信息");
        puts("6 保存学生信息");
        puts("7 退出");

        printf("选择功能：");
        ch = getchar();
        clear_input();

        if ('7' == ch)
            break;

        switch (ch)
        {
            case '1':
                add_students(students);
                rank_students_by_scores(students);
                break;
            case '2':
                printf("输入需要删除信息的学生学号：");
                scanf("%d", &number);
                clear_input();
                ret_val = delete_student_by_number(students, number);
                if (-1 == ret_val)
                    printf("没有 %d 学号的学生信息。\n", number);
                else
                    printf("已删除 %d 学号的学生信息。还有 %d 个学生信息。\n",
                            number, ret_val);
                rank_students_by_scores(students);
                break;
            case '3':
                printf("输入需要修改信息的学生学号：");
                scanf("%d", &number);
                clear_input();
                ret_val = seek_student_by_number(students, number);
                if (-1 == ret_val)
                    printf("没有 %d 学号的学生信息。\n", number);
                else if (-1 != loc && students[ret_val].stu_class != teachers[loc].tea_class)
                {
                    printf("%d 班老师无法修改 %d 班学生的信息。\n",
                            teachers[loc].tea_class,
                            students[ret_val].stu_class);
                }
                else
                {
                    modify_student_by_location(students, ret_val);
                    printf("%d 学号的学生信息修改成功。\n", number);
                    print_student_header();
                    print_a_student(students + ret_val);
                    rank_students_by_scores(students);
                }
                break;
            case '4':
                seek_students(students);
                clear_input();
                break;
            case '5':
                show_students(students);
                clear_input();
                break;
            case '6':
                rank_students_by_scores(students);
                write_students(students);
                break;
            /* case '7': */
            /*     return; */
            default:
                printf("%c 无效输入。\n", ch);
                break;
        }
        if (!(ch == '5' || ch == '1' || ch == '4'))
        {
            printf("按回车进行下一步");
            getchar();
        }

    }
}

void student_menu_by_student(struct student * students, int loc)
{
    char ch;
    char pwd[MAXPWD];
    while (1)
    {
        printf("=== 学生菜单 ===\n");
        printf("\t\t欢迎%s同学登入\n", students[loc].name);
        puts("1 修改密码");
        puts("2 查看学生信息");
        puts("3 显示所有学生信息");
        puts("4 保存学生信息");
        puts("5 退出");

        printf("选择功能：");
        ch = getchar();
        clear_input();

        if ('5' == ch)
            break;

        switch (ch)
        {
            case '1':
                printf("输入新密码：");
                s_gets(pwd, MAXPWD);
                strcpy(students[loc].password, pwd);
                printf("密码修改成功。\n");
                print_student_header();
                print_a_student(students + loc);
                break;
            case '2':
                print_student_header();
                print_a_student(students + loc);
                break;
            case '3':
                show_students(students);
                clear_input();
                break;
            case '4':
                write_students(students);
                break;
            /* case 5: */
            /*     exit(EXIT_SUCCESS); */
            default:
                printf("无效输入。\n");
                break;
        }
        if (ch != '3')
        {
            printf("按回车进行下一步");
            getchar();
        }
    }
}

void teacher_menu(struct teacher * teachers, int loc)
{
    if (is_admin)
    {
        teacher_menu_by_admin(teachers);
    }
    else
    {
        teacher_menu_by_teacher(teachers, loc);
    }
}

void teacher_menu_by_admin(struct teacher * teachers)
{
    char ch;
    int number, ret_val;
    while (1)
    {
        printf("=== 老师菜单 ===\n");
        puts("1 添加老师信息");
        puts("2 删除老师信息");
        puts("3 修改老师信息");
        puts("4 显示老师信息");
        puts("5 保存老师信息");
        puts("6 退出");

        printf("选择功能：");
        ch = getchar();
        clear_input();

        if ('q' == ch)
            break;

        switch (ch)
        {
            case '1':
                add_teachers(teachers);
                break;
            case '2':
                printf("输入需要删除信息的老师序号：");
                scanf("%d", &number);
                clear_input();
                ret_val = delete_teacher_by_number(teachers, number);
                if (-1 == ret_val)
                    printf("没有 %d 序号的老师信息。\n", number);
                else
                    printf("已删除 %d 序号的老师信息。还有 %d 个老师信息。\n",
                            number, ret_val);
                break;
            case '3':
                printf("输入需要修改信息的老师序号：");
                scanf("%d", &number);
                clear_input();
                ret_val = modify_teacher_by_number(teachers, number);
                if (-1 == ret_val)
                    printf("没有 %d 序号的老师信息。\n", number);
                else
                {
                    printf("%d 序号的老师信息修改成功。\n", number);
                    print_teacher_header();
                    print_a_teacher(teachers + ret_val);
                }
                break;
            case '4':
                print_teachers(teachers);
                break;
            case '5':
                write_teachers(teachers);
                break;
            case '6':
                return;
            default:
                printf("无效输入。\n");
                break;
        }
        if (ch != '1')
        {
            printf("按回车进行下一步");
            getchar();
        }
    }
}

void teacher_menu_by_teacher(struct teacher * teachers, int loc)
{
    char ch;
    char pwd[MAXPWD];
    while (1)
    {
        printf("=== 老师菜单 ===\n");
        printf("\t\t欢迎%s老师登入\n", teachers[loc].name);
        puts("1 修改老师信息");
        puts("2 显示老师信息");
        puts("3 保存老师信息");
        puts("4 退出");

        printf("选择功能：");
        ch = getchar();
        clear_input();

        if ('q' == ch)
            break;

        switch (ch)
        {
            case '1':
                printf("输入新密码：");
                s_gets(pwd, MAXPWD);
                strcpy(teachers[loc].password, pwd);
                printf("密码修改成功。\n");
                print_teacher_header();
                print_a_teacher(teachers + loc);
                break;
            case '2':
                print_teacher_header();
                print_a_teacher(teachers + loc);
                break;
            case '3':
                write_teachers(teachers);
                break;
            case '4':
                return;
            default:
                printf("无效输入。\n");
                break;
        }
        printf("按回车进行下一步");
        getchar();
    }
}
void teacher_student_menu(struct teacher * teachers, 
        struct student * students, int loc)
{
    char ch;
    while (1)
    {
        puts("1 学生菜单");
        puts("2 老师菜单");

        printf("选择功能（按 q 退出）：");
        ch = getchar();
        clear_input();

        if ('q' == ch)
            exit(EXIT_SUCCESS);

        switch (ch)
        {
            case '1':
                student_menu(students, teachers, loc); /* -1 以为没有学生登入 */
                break;
            case '2':
                teacher_menu(teachers, loc);
                break;
        }
    }
}

void seek_students(struct student * students)
{
    char ch;
    int input, ret_val;
    char name[MAXNAME];
    while (1)
    {
        printf("1 通过学号查找");
        printf("\n2 通过姓名查找");
        printf("\n3 通过班级查找\n");

        printf("输入需要查找的方式：");
        ch = getchar();
        clear_input();
        switch (ch)
        {
            case '1':
                printf("输入要查询学生的学号：");
                scanf("%d", &input);
                clear_input();
                ret_val = seek_student_by_number(students, input);
                if (-1 == ret_val)
                    printf("没有查询到学号 %d 的学生信息。\n", input);
                else
                {
                    print_student_header();
                    print_a_student(students + ret_val);
                }
                break;
            case '2':
                printf("输入要查询学生的姓名：");
                s_gets(name, MAXNAME);
                ret_val = seek_students_by_name(students, name);
                if (ret_val)
                    printf("共查询到 %d 个学生信息。\n", ret_val);
                else
                    printf("没有找到该学生信息。\n");
                break;
            case '3':
                printf("输入要查询的班级：");
                scanf("%d", &input);
                clear_input();
                ret_val = seek_students_by_class(students, input);
                if (ret_val)
                    printf("共查询到 %d 个学生信息。\n", ret_val);
                else
                    printf("没有查询到该班级。\n");
                break;
            case 'q':
                return;
            default:
                printf("无效输入，请重新输入。\n");
        }
        printf("按 q 退出查询，其它继续查询：");
        if (getchar() == 'q')
            break;
    }
}

void show_students(struct student * students)
{
    char ch;
    struct student temp_stus[MAXSTU] = {0};
    int subject;
    while (1)
    {
        printf("1 按学号顺序显示\n");
        printf("2 按名次显示\n");
        printf("3 按某学科(可选)成绩顺序显示\n");

        printf("输入需要显示的方式：");
        ch = getchar();
        clear_input();
        switch (ch)
        {
            case '1':
                sort_students_by_number(students);
                print_students(students);
                break;
            case '2':
                copy_students_array(temp_stus, students);
                rank_students_by_scores(temp_stus);
                print_students(temp_stus);
                break;
            case '3':
                copy_students_array(temp_stus, students);
                printf("输入需要排名的学科：");
                scanf("%d", &subject);
                clear_input();
                sort_students_by_subject(temp_stus, subject);
                print_students(temp_stus);
                printf("该学科的平均为：%.2f\n", 
                        1.0 * sum_scores_by_subject(temp_stus, subject) / stucount);
                break;
            case 'q':
                return;
            default:

                printf("无效输入，请重新输入。\n");
        }
        printf("按 q 退出查询，enter 继续查询：");
        if (getchar() == 'q')
            break;
    }

}



