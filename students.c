#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "students.h"
#include "utils.h"

int stucount = 0; /* 全局变量不要定义在 .h 文件中 */

int sum_scores_by_student(struct student *, int);

/*
 * Function:
 * Description: 连续添加学生信息（学号不可重复）
 * Input: 
 * Return: 
 * TODO: 学号去重
 */
void add_students(struct student * students)
{
    puts("输入新学生姓名：");
    puts("按 [enter] 停止录入学生信息");
    while (stucount < MAXSTU && 
            s_gets(students[stucount].name, MAXNAME) != NULL &&
            students[stucount].name[0] != '\0')
    {
        students[stucount].number = students[stucount - 1].number + 1;
        puts("输入学生密码：");
        s_gets(students[stucount].password, MAXPWD);
        puts("输入年龄、班级：");
        puts("输入数学、C 语言和语文成绩：");

        scanf("%d%d%d%d%d",
                &students[stucount].age,
                &students[stucount].stu_class,
                students[stucount].stu_grade.scores,
                students[stucount].stu_grade.scores + 1,
                students[stucount].stu_grade.scores + 2);

        stucount++;

        clear_input();

        if (stucount < MAXSTU)
            puts("输入下一个学生姓名：");
    }

    if (stucount == MAXSTU)
    {
        fputs("学生信息文件已满\n", stderr);
        return;
        /* exit(EXIT_FAILURE); */
    }

}

void rank_students_by_scores(struct student * students)
{
    sort_students_by_scores(students);

    for (int i = 0; i < stucount; i++)
    {
        /* 分数相同，名次相同 */
        if (i > 0 &&
                sum_scores_by_student(students, i-1) ==
                sum_scores_by_student(students, i))
        {
            students[i].stu_grade.rank = students[i-1].stu_grade.rank;
            continue;
        }

        students[i].stu_grade.rank = i + 1;
    }
}

void print_students(struct student * students)
{
    print_student_header();
    for (int i = 0; i < stucount; i++)
    {
        print_a_student(students + i);
    }
}

void print_student_header()
{
    printf("学号\t姓名\t密码\t年龄\t班级\t数学\tC 语言\t语文\t名次\n");
}

void print_a_student(struct student * student)
{
        printf("%d\t%s\t%s\t%d\t%d\t%d\t%d\t%d\t%d\n",
                student->number, student->name, student->password,
                student->age, student->stu_class,
                student->stu_grade.scores[0],
                student->stu_grade.scores[1],
                student->stu_grade.scores[2],
                student->stu_grade.rank);
}

/*
 * Return: 信息所在的位置，-1 表示没有找到
 */
int seek_student_by_number(struct student * students, int number)
{
    for (int i = 0; i < stucount; i++)
    {
        if ( students[i].number == number )
            return i;
    }
    return -1; // 没有找到
}

/*
 * Return: 返回查询的个数；0 表示没有查询到
 */
int seek_students_by_name(struct student * students, char * name)
{
    int found = 0;
    for (int i = 0; i < stucount; i++)
    {
        if (0 == strcmp(students[i].name, name))
        {
            if (found == 0)
                print_student_header();
            print_a_student(students + i);
            found++;
        }
    }

    return found;
}

/*
 * Return: 返回查询的个数；0 表示没有查询到
 */
int seek_students_by_class(struct student * students, int stu_class)
{
    int found = 0;
    for (int i = 0; i < stucount; i++)
    {
        if (students[i].stu_class == stu_class)
        {
            if (found == 0)
                print_student_header();
            print_a_student(students + i);
            found++;
        }
    }

    return found;
}

/* 
 * Return: 修改学生的位置，-1 表示没有找到该学生信息
 */
void modify_student_by_location(struct student * students, int location)
{
    /* int location = seek_student_by_number(students, number); */

    /* 没有找到该学号的学生信息 */
    /* if (-1 == location) */
    /* { */
    /*     return -1; */ 
    /* } */

    puts("输入学生姓名：");
    s_gets(students[location].name, MAXNAME);
    puts("输入学生密码：");

    s_gets(students[location].password, MAXPWD);
    puts("输入年龄、班级：");
    puts("输入数学、C 语言和语文成绩：");

    scanf("%d%d%d%d%d",
            &students[location].age,
            &students[location].stu_class,
            students[location].stu_grade.scores,
            students[location].stu_grade.scores + 1,
            students[location].stu_grade.scores + 2);
    clear_input();

    /* return location; */
}

void swap(struct student * array, int xp, int yp)
{
    struct student temp = *(array + xp);
    *(array + xp) = *(array + yp);
    *(array + yp) = temp;
}

void sort_students_by_number(struct student * students)
{
    for (int i = 0; i < stucount - 1; i++)
    {
        int swapped = 0;
        for (int j = 0; j < stucount - i - 1; j++)
        {
            if (students[j].number > students[j+1].number)
            {
                swap(students, j , j+1);
                swapped = 1;
            }
        }

        if (!swapped)
            break;
    }
}

int sum_scores_by_student(struct student * students, int location)
{
    int sum = 0;
    for (int i = 0; i < 3; i++)
    {
        sum += students[location].stu_grade.scores[i];
    }
    return sum;
}

void sort_students_by_scores(struct student * students)
{
    for (int i = 0; i < stucount - 1; i++)
    {
        int swapped = 0;
        for (int j = 0; j < stucount - i - 1; j++)
        {
            if (sum_scores_by_student(students, j) <
                    sum_scores_by_student(students, j+1))
            {
                swap(students, j, j+1);
                swapped = 1;
            }
        }
        if (!swapped)
            break;
    }
}

int sum_scores_by_subject(struct student * students, int subject)
{
    int sum = 0;
    for (int i = 0; i < stucount; i++)
    {
        sum += students[i].stu_grade.scores[subject];
    }

    return sum;
}

/*
 * Description: 按某学科(可选)成绩顺序显示，并在最后显示出此科平均分
 * Input: subject：0 -> 数学；1 -> C 语言；2 -> 语文
 */
void sort_students_by_subject(struct student * students, int subject)
{
    for (int i = 0; i < stucount - 1; i++)
    {
        int swapped = 0;
        for (int j = 0; j < stucount - i - 1; j++)
        {
            if (students[j].stu_grade.scores[subject] <
                    students[j+1].stu_grade.scores[subject])
            {
                swap(students, j, j+1);
                swapped = 1;
            }
        }

        if (!swapped)
            break;
    }
}

/*
 * Return: 成功则返回剩余多少学生信息；失败则返回 -1
 */
int delete_student_by_number(struct student * students, int number)
{

    int location = seek_student_by_number(students, number);

    /* 没有找到该学生信息 */
    if (-1 == location)
        return -1;

    for (int i = location; i < stucount; i++)
        students[i] = students[i+1];
    stucount--;

    return stucount;
}

void read_students(struct student * students)
{
    FILE * pstudents;
    int size = sizeof (struct student);

    if ((pstudents = fopen("data/students.dat", "rb")) == NULL)
    {
        fputs("不能打开学生信息文件\n", stderr);
        exit(1);
    }

    rewind(pstudents); /* go to start of file */
    while (stucount < MAXSTU && 
            fread((students + stucount), size, 1, pstudents) == 1)
    {
        stucount++;
    }

    printf("文件读取成功，已录入 %d 个学生。\n", stucount);
    fclose(pstudents);
}

void write_students(struct student * students)
{
    /* extern int stucount; */
    FILE * pstudents;
    int size = sizeof(struct student);

    if ((pstudents = fopen("data/students.dat", "wb")) == NULL)
    {
        fputs("不能打开学生信息文件\n", stderr);
        exit(1);
    }

    rewind(pstudents); /* go to start of file */

    if (stucount > 0)
    {
        fwrite(students, size, stucount, pstudents);
        puts("学生信息保存成功。");
    }
    else
        puts("没有学生信息要保存。");

    fclose(pstudents);
}

void copy_students_array(struct student * dest, struct student * src)
{
    for (int i = 0; i < stucount; i++)
        dest[i] = src[i];
}
