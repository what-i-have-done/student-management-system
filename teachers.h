#define MAXTEA 200
#define MAXNAME 128
#define MAXPWD 32

struct teacher
{
    int number; /* 教师序号 */
    char name[128]; /* 教师姓名 */
    char password[32]; /* 教师密码 */
    int tea_class; /* 教师所在的班级 */
};

extern void read_teachers(struct teacher *);
extern void write_teachers(struct teacher *);
extern void add_teachers(struct teacher *);
extern void print_teachers(struct teacher *);
extern void print_teacher_header();
extern void print_a_teacher(struct teacher *);
extern int seek_teacher_by_number(struct teacher *, int);
extern int modify_teacher_by_number(struct teacher *, int);
extern int delete_teacher_by_number(struct teacher *, int);

extern int teacount;
