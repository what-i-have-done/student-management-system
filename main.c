#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "students.h"
#include "teachers.h"
#include "display.h"

int main(int argc, char *argv[])
{
#if 0
    struct student students[MAXSTU] = {0};

    /* 录入学生测试 */
    read_students(students);
    print_students(students);
    add_students(students);
    printf("%d\n", stucount);
    print_students(students);
    /* write_students(students); // 保存学生信息 */
    /* printf("%d\n", seek_student_by_number(students, 0, stucount)); */
    /* seek_students_by_name(students, "zhao", stucount); */
    /* seek_students_by_class(students, 2); */
    /* modify_student_by_number(students, 2); */
    /* sort_students_by_number(students); */
    /* sort_students_by_scores(students); */
    /* sort_students_by_subject(students, 2); */
    /* print_students(students); */
    /* printf("语文总分：%d\n", sum_scores_by_subject(students, 2)); */
    /* write_students(students); */
    /* printf("%d\n", delete_student_by_number(students, 1)); */
    /* print_students(students); */

    /* 录入老师信息测试 */
    struct teacher teachers[MAXTEA] = {0};

    read_teachers(teachers);
    add_teachers(teachers);
    printf("%d\n", teacount);
    print_teachers(teachers);
    /* write_teachers(teachers); */
    /* printf("%d\n", seek_teacher_by_number(teachers, 3)); */
    /* modify_teacher_by_number(teachers, 2); */
    /* print_teachers(teachers); */
#endif

#if 1
    struct student students[MAXSTU] = {0};
    struct teacher teachers[MAXTEA] = {0};
    read_teachers(teachers);
    read_students(students);
    if (argc == 1)
    {
        login_menu(students, teachers);
        exit(EXIT_SUCCESS);

    }

    if (argc != 2)
    {
        printf("请加上 --admin 参数运行。\n");
        exit(EXIT_FAILURE);
    }

    if (!strcmp(argv[1], "--admin"))
    {
        admin_menu(students, teachers);
    }
    else
    {
        printf("请加上 --admin 和 --user 参数运行。\n");
        exit(EXIT_FAILURE);
    }
#endif
    /* read_students(students); */
    /* print_students(students); */
    /* student_login(students); */
    /* student_menu(students); */

    /* read_teachers(teachers); */
    /* print_teachers(teachers); */
    /* teacher_login(teachers); */
    /* teacher_menu(teachers); */

    return 0;
}
