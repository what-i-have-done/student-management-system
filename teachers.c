#include <stdio.h>
#include <stdlib.h>
#include "teachers.h"
#include "utils.h"

int teacount = 0; /* 老师计数（全局变量）*/

void add_teachers(struct teacher * teachers)
{
    puts("输入老师姓名：");
    puts("按 [enter] 停止录入老师信息");
    while (teacount < MAXTEA &&
            s_gets(teachers[teacount].name, MAXNAME) != NULL &&
            teachers[teacount].name[0] != '\0')
    {
        teachers[teacount].number = teachers[teacount - 1].number + 1;
        puts("输入教师密码：");
        s_gets(teachers[teacount].password, MAXPWD);
        puts("输入班级：");
        scanf("%d", &teachers[teacount].tea_class);

        teacount++;

        clear_input();

        if (teacount < MAXTEA)
            puts("请输入下一位老师的姓名：");
    }

    if (teacount == MAXTEA)
    {
        fputs("学生信息文件已满\n", stderr);
        return;
    }

}

void print_teachers(struct teacher * teachers)
{
    print_teacher_header();
    for (int i = 0; i < teacount; i++)
    {
        print_a_teacher(teachers + i);
    }
}

void print_teacher_header()
{
    printf("序号\t姓名\t密码\t班级\n");
}

void print_a_teacher(struct teacher * teacher)
{
        printf("%d\t%s\t%s\t%d\n",
                teacher->number, teacher->name,
                teacher->password, teacher->tea_class);
}

int seek_teacher_by_number(struct teacher * teachers, int number)
{
    for (int i = 0; i < teacount; i++)
    {
        if ( teachers[i].number == number )
            return i;
    }
    return -1; // 没有找到
}

int modify_teacher_by_number(struct teacher * teachers, int number)
{
    int location = seek_teacher_by_number(teachers, number);

    /* 没有找到该序号的老师信息 */
    if (-1 == location)
    {
        return -1; 
    }

    /* int num; */
    /* puts("输入老师序号："); */
    /* scanf("%d", &num); */
    /* clear_input(); */
    /* while (-1 != seek_teacher_by_number(teachers, num)) */
    /* { */
    /*     puts("教师序号已存在，请重新输入："); */
    /*     scanf("%d", &num); */
    /*     clear_input(); */
    /* } */
    /* teachers[location].number = num; */
    puts("输入教师姓名：");
    s_gets(teachers[location].name, MAXNAME);
    puts("输入教师密码：");
    s_gets(teachers[location].password, MAXPWD);
    puts("输入班级：");
    scanf("%d", &teachers[location].tea_class);
    clear_input();

    return location;
}

int delete_teacher_by_number(struct teacher * teachers, int number)
{
    int location = seek_teacher_by_number(teachers, number);

    /* 没有找到该老师信息 */
    if (-1 == location)
        return -1;

    for (int i = location; i < teacount; i++)
        teachers[i] = teachers[i+1];
    teacount--;

    return teacount;
}

void read_teachers(struct teacher * teachers)
{
    FILE * pteachers;
    int size = sizeof (struct teacher);

    if ((pteachers = fopen("data/teachers.dat", "rb")) == NULL)
    {
        fputs("不能打开老师信息文件\n", stderr);
        exit(1);
    }

    rewind(pteachers); /* go to start of file */
    while (teacount < MAXTEA && 
            fread((teachers + teacount), size, 1, pteachers) == 1)
    {
        teacount++;
    }

    printf("文件读取成功，已录入 %d 个老师。\n", teacount);
    fclose(pteachers);
}

void write_teachers(struct teacher * teachers)
{
    FILE * pteachers;
    int size = sizeof(struct teacher);

    if ((pteachers = fopen("data/teachers.dat", "wb")) == NULL)
    {
        fputs("不能打开老师信息文件\n", stderr);
        exit(1);
    }

    rewind(pteachers); /* go to start of file */

    if (teacount > 0)
    {
        fwrite(teachers, size, teacount, pteachers);
        puts("老师信息保存成功。");
    }
    else
        puts("没有老师信息要保存。");

    fclose(pteachers);
}
