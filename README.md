## 程序考虑

### 数据结构定义

<pre>
学生信息：
    基本数据信息:
        学号:  int
        姓名:  char   128
        密码:  char   32
        年龄:  int
        班级:  int
    成绩信息:
        数学成绩:  int
        C语言成绩:  int
        语文成绩:  int
        名次:  int
</pre>

学生的结构定义如下:

```c
#define MAXNAME 128
#define MAXPWD 32

struct grade
{
    int scores[3];
    int rank;
};

struct student
{
    int number; // 学号
    char name[MAXNAME]; // 姓名
    char password[MAXPWD]; // 密码
    int age;
    int stu_class;
    struct grade stu_grade;
};
```

<pre>
教师信息:
    教员序号:  int
    教员姓名:  char  128
    教员密码:  char  32
    所任班级:  int
</pre>

老师结构如下

```c
#define MAXTEA 200
#define MAXNAME 128
#define MAXPWD 32

struct teacher
{
    int number; /* 教师序号 */
    char name[128]; /* 教师姓名 */
    char password[32]; /* 教师密码 */
    int tea_class; /* 教师所在的班级 */
};
```
### 程序结构

<pre>
.
├-- data
    ├── students.dat
    └──teachers.dat
├-- students.c   学生相关操作
├-- students.h   学生相关定义
├-- teachers.c   老师相关操作
├-- teachers.h   老师相关定义
├-- display.c    菜单显示和权限控制
├-- display.h
├-- login.c      学生、老师和管理登入
├-- login.h
└──main.c       程序入口

</pre>

> 信息运行是把信息保存在内存中，系统退出后信息保存至文件

在程序运行时，从文件中读取数据保存到结构体数组中，退出系统时，询问是否保存到文件，如果个保存，就将数组写进文件。

学生操作如下：

* 录入学生信息
* 删除学生信息
* 修改学生信息
* 查找学生信息
    * 按学号查找
    * 按姓名查找（可以查出所有同名的学生）
    * 按班级查找
* 显示所有学习信息
    * 按学号排序
    * 按名次排序
    * 按某学科顺序显示，并显示平均分
* 保存学生信息

老师操作如下：

* 录入老师信息
* 删除老师信息
* 修改老师信息
* 显示老师信息

要求：

* 学生学号和老师序号不能重复
* 登入成功后，显示『显示 XX 老师（同学）登入』
* 登入超过 3 次后自动退出

### 一些问题

**问：** 学生（老师）计数

由于不能用 `sizeof` 去计算已有多少学生，所以定义一个全局变量 `stucount` 来存储学生数量（老师计数同理）。

解决一：

```c
struct student students[MAXSTU] = {0};

while (students[stucount].name[0] != '\0')
    stucount++;
```

解决二：

```c
FILE * pstudents;
int size = sizeof (struct student);
pstudents = fopen("data/students.dat", "ab");

while (stucount < MAXSTU && 
        fread((students + stucount), size, 1, pstudents) == 1)
    stucount++;
```

其实 fread() 函数返回它读的数量个数。

><pre>RETURN VALUE
>   On  success, fread() and fwrite() return the number of items read or written.
>   This number equals the number of bytes transferred only when size is 1.
>   If an error occurs, or the end of the file is reached, the return value is a short item count (or zero).</pre>

所以可以直接这样：

```c
stucount = fread((students + stucount), size, MAXSTU, pstudents);
```

**问：** 存放学生的数组是定义全局变量还是传参？

最终还是考虑用到传参。

**问：** 输入缓冲区（input buffer）问题

```c
void clear_input()
{
    while (getchar() != '\n')
        continue;               /* clear input line */
}
```

**问：** 字符串输入

```c
char * s_gets(char * st, int n)
{
    char * ret_val;
    char * find;

    ret_val = fgets(st, n, stdin);
    if (ret_val)
    {
        find = strchr(st, '\n');
        if (find)
            *find = '\0';
        else
            while (getchar() != '\n')
                continue;
    }
    return ret_val;
}
```

## 程序完善

- [x] 学生学号和教师学号自动递增
- [x] 不同班级老师不能修改不同班级学生的信息
- [ ] 年龄、班级和分数加上限定

## 程序拓展

* 本程序是用数组做的（暂时学到数组），也可以改写成链表
* 程序也可以加入班级的考量

    可以用二维数组，或链表数组做，应该更好做一些

* 更深入的再加入年级

`Yes` or `No` in loop:

* https://stackoverflow.com/questions/18627163/yes-no-loop-in-c
* https://www.daniweb.com/programming/software-development/threads/272069/yes-and-no-choice-in-c-programing
* https://cboard.cprogramming.com/c-programming/113295-how-do-option-yes-no.html

Flush input buffer:
* https://stackoverflow.com/questions/7898215/how-to-clear-input-buffer-in-c

Menu:
* https://stackoverflow.com/questions/10571127/how-to-write-a-console-menu-in-ansi-iso-c

* https://stackoverflow.com/questions/17381867/declaring-arrays-in-c-language-without-initial-size
* https://stackoverflow.com/questions/7263723/struct-with-array-of-structs-of-unknown-size

Initialization of structure

* http://www.ex-parrot.com/~chris/random/initialise.html
* https://stackoverflow.com/questions/22465908/how-could-i-initialize-a-array-of-struct-to-null

全局变量声明：
* https://stackoverflow.com/questions/8108634/global-variables-in-header-file

数组排序：
* https://stackoverflow.com/questions/35376769/sorting-one-array-into-another-c

交换结构体数组元素：
* https://stackoverflow.com/questions/2138872/swapping-elements-in-an-array-of-structs?rq=1

冒泡排序优化：
* https://www.ritambhara.in/optimized-bubble-sort-algorithm/
* https://algorithms.tutorialhorizon.com/optmized-bubble-sort-java-implementation/
* https://www.geeksforgeeks.org/bubble-sort/
* https://en.wikipedia.org/wiki/Bubble_sort
* https://stackoverflow.com/questions/16195092/optimized-bubble-sort-java
* https://youtu.be/32pF2cDbaSw
* https://youtu.be/8Ug-a3IhUaE

从数组中移除元素
* https://www.geeksforgeeks.org/delete-an-element-from-array-using-two-traversals-and-one-traversal/
* https://stackoverflow.com/questions/15821123/removing-elements-from-an-array-in-c

Git
* https://stackoverflow.com/questions/12481639/remove-files-from-git-commit
* https://dev.to/iamafro/how-to-merge-a-specific-commit-into-another-branch--oak
*
exit() vs return
* https://stackoverflow.com/questions/461449/return-statement-vs-exit-in-main
* https://stackoverflow.com/questions/3463551/what-is-the-difference-between-exit-and-return

Menu driver
https://codeography.wordpress.com/2016/08/07/a-menu-driven-program-using-infinite-loop-and-switch/
https://www.youtube.com/watch?v=w3-0Ut0QQtU

tar
* https://superuser.com/questions/559339/tar-exclude-git-cannot-stat-no-such-file-or-directory
* https://unix.stackexchange.com/questions/340996/tar-is-not-obeying-exclude-vcs

Misc
* https://en.wikipedia.org/wiki/Truth_table#Logical_NAND

Makefile
* https://courses.cs.washington.edu/courses/cse451/99wi/Section/makeintro.html
* https://gcc.gnu.org/onlinedocs/gcc/Warning-Options.html
