#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "students.h"
#include "teachers.h"
#include "display.h"
#include "utils.h"

void admin_login()
{
    int count = 0;
    char username[MAXNAME], pwd[MAXPWD];
    printf("输入管理员用户名：");
    s_gets(username, MAXNAME);
    printf("输入管理员密码：");
    s_gets(pwd, MAXPWD);
    int match = strcmp(username, "admin");
    while (!(!match && !strcmp(pwd, "admin")))
    {
        if (match)
        {
            printf("用户名不正确。\n");
            count++;
            if (count == 3)
                exit(EXIT_FAILURE);
            printf("请重新输入用户名：");
            s_gets(username, MAXPWD);
            printf("请重新输入密码：");
            s_gets(pwd, MAXPWD);
            continue;
        }
        printf("用户名和密码不匹配。\n");
        count++;
        if (count == 3)
            exit(EXIT_FAILURE);
        printf("请重新输入用户名：");
        s_gets(username, MAXPWD);
        printf("请重新输入密码：");
        s_gets(pwd, MAXPWD);
    }

    is_admin = 1;
    is_teacher = 1;
}

int student_login(struct student * students)
{
    printf("== 学生登入 ==\n");
    int number, count = 0;
    char pwd[MAXPWD];
    printf("输入学生学号：");
    scanf("%d", &number);
    clear_input();
    printf("输入学生密码：");
    s_gets(pwd, MAXPWD);
    int location = seek_student_by_number(students, number);
    while ( -1 == location || strcmp(pwd, students[location].password))
    {
        if (-1 == location)
        {
            printf("没有找到该学号学生的信息。\n");
            count++;
            if (count == 3)
                exit(EXIT_FAILURE);
            printf("请重新输入学号：");
            scanf("%d", &number);
            clear_input();
            printf("请重新输入密码：");
            s_gets(pwd, MAXPWD);
            location = seek_student_by_number(students, number);
            continue;
        }
        printf("学生学号和密码不匹配。\n");
        count++;
        if (count == 3)
            exit(EXIT_FAILURE);
        printf("请重新输入学号：");
        scanf("%d", &number);
        clear_input();
        printf("请重新输入密码：");
        s_gets(pwd, MAXPWD);
        location = seek_student_by_number(students, number);
    }

    is_teacher = 0; /* 学生登入 */

    return location;

}

int teacher_login(struct teacher * teachers)
{
    printf("== 教师登入 ==\n");
    int number, count = 0;
    char pwd[MAXPWD];
    printf("输入老师序号：");
    scanf("%d", &number);
    clear_input();
    printf("输入老师密码：");
    s_gets(pwd, MAXPWD);
    int location = seek_teacher_by_number(teachers, number);
    while ( -1 == location || strcmp(pwd, teachers[location].password))
    {
        if (-1 == location)
        {
            printf("没有找到该序号老师的信息。\n");
            count++;
            if (count == 3)
                exit(EXIT_FAILURE);
            printf("请重新输入序号：");
            scanf("%d", &number);
            clear_input();
            printf("请重新输入密码：");
            s_gets(pwd, MAXPWD);
            location = seek_teacher_by_number(teachers, number);
            continue;
        }
        printf("老师序号和密码不匹配。\n");
        count++;
        if (count == 3)
            exit(EXIT_FAILURE);
        printf("请重新输入序号：");
        scanf("%d", &number);
        clear_input();
        printf("请重新输入密码：");
        s_gets(pwd, MAXPWD);
        location = seek_teacher_by_number(teachers, number);
    }

    is_teacher = 1; /* 老师登入 */
    return location;
}

